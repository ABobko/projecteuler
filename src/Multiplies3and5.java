/*
    Multiplies of 3 and 5
    problem 1:

        If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
        The sum of these multiples is 23.
        Find the sum of all the multiples of 3 or 5 below 1000.

    Exercise can be found at:   https://projecteuler.net/problem=1

    More exercises:             https://projecteuler.net/archives

*/

package archives.net.projecteuler;

public class Multiplies3and5 {

    public static void main(String[] args) {
        int checkMaxIntValue = Integer.MAX_VALUE;
        System.out.println(checkMaxIntValue+"<---the upper range of int, just to make sure " +
                "we don't need to use BigInteger");
        System.out.println(sumOfMultiplies(1000)+"<--this is our result");

    }

    public static int sumOfMultiplies(int range){
        int result=0;
        for (int i=0;i<=range;i++){
            if(i%3==0||i%5==0){
                result+=i;
            }
        }
        return result;
    }
}

