/*
    Largest Prime Factor

    problem 3:

        The prime factors of 13195 are 5, 7, 13 and 29.
        What is the largest prime factor of the number 600851475143?

    Exercise can be found at:   https://projecteuler.net/problem=3

    More exercises:             https://projecteuler.net/archives
*/

package archives.net.projecteuler;

import java.math.BigInteger;


public class LargestPrimeFactor {

    public static void main(String[] args) {
        System.out.println(maxPrimeFactor("600851475143"));
    }

    public static int maxPrimeFactor(String checkThisNumber){
         int upperRange = new BigInteger(checkThisNumber).sqrt().intValue();
         BigInteger checkThisBigInt = new BigInteger(checkThisNumber);
         int largestPrimeFactor=-1;
         for (int i=2;i<=upperRange;i++){
             if(isPrime(i)){
                 if (BigInteger.ZERO.equals(checkThisBigInt.mod(new BigInteger(String.valueOf(i))))){
                       largestPrimeFactor=i;
                 }
             }
         }
         return largestPrimeFactor;
    }

    public static boolean isPrime(int checkThisNumber){
        if (checkThisNumber==1){
            return false;
        }
        if (checkThisNumber==2||checkThisNumber==3||checkThisNumber==5||checkThisNumber==7){
            return true;
        }
        if(checkThisNumber%2==0||checkThisNumber%3==0||checkThisNumber%5==0){
            return false;
        }
        int range =(int) Math.sqrt(checkThisNumber);
        for (int i=7;i<=range;i+=2){
            if (checkThisNumber%i==0){
                return false;
            }
        }
        return true;
    }
}
